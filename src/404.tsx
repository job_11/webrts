import SentimentVeryDissatisfiedIcon from '@mui/icons-material/SentimentVeryDissatisfied';

export default function NoMatch() {
    return (
        <>
            <div
                style={{
                    position: 'absolute',
                    left: '50%',
                    top: '50%',
                    transform: 'translate(-50%, -50%)',
                    display: "flex",
                }}
            >
                <SentimentVeryDissatisfiedIcon style={{ fontSize: 55 }} />
                <p style={{ marginTop: "15px", fontSize: "20px", marginLeft: 10 }}>
                    Sorry, there's nothing here.
                </p>
            </div>

        </>
    );
};


