import * as React from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import { grey } from '@mui/material/colors';
import TextField from '@mui/material/TextField';
import DialogActions from '@mui/material/DialogActions';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import dayjs from 'dayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import axios from 'axios';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import FormControl from '@mui/material/FormControl';

import './App.css'
import {
    useMutation,
    useQueryClient,
} from 'react-query'


export default function RegisterPage({ isOpen, isRow, handleAlert, handleIsOpen, editShow, handleIsRow }: any) {
    const api = "http://127.0.0.1:8000/api";
    const queryClient = useQueryClient();

    const [firstName, setFirstName] = React.useState('');
    const [middleName, setMiddleName] = React.useState('');
    const [lastName, setLastName] = React.useState('');
    const [gender, setGender] = React.useState('');
    const [dOB, setDOB] = React.useState<any>(dayjs('2024-01-01'));
    const [age, setAge] = React.useState(0);

    React.useEffect(() => { setFirstName(isRow.Firstname) }, [isRow.Firstname])
    React.useEffect(() => { setMiddleName(isRow.Middlename) }, [isRow.Firstname])
    React.useEffect(() => { setLastName(isRow.Lastname) }, [isRow.Lastname])
    React.useEffect(() => { setGender(isRow.Gender) }, [isRow.Gender])
    React.useEffect(() => { setDOB(dayjs(isRow.DateOfBirth)) }, [isRow.DateOfBirth])
    React.useEffect(() => { setAge(isRow.Age) }, [isRow.Age])


    const pInfoSave: any = useMutation(
        async (data) => {
            return await axios.post(api + "/register", data, {
                headers: {
                    Accept: "application/json",
                },
            });
        },
        {
            onSuccess: (res: any) => {
                clear();
                handleAlert(res.data.message)
                handleClose();
                queryClient.invalidateQueries("pInfoData");
            },
            onError: (error: any) => {
                handleAlert(error.data.message)
                console.log("error" + error);
            },
        }
    );
    const handleSave = (data: any) => {
        // let dt = moment(new Date(dOB)).format('YYYY-MM-DD')
        pInfoSave.mutate(data);
    }

    const clear = () => {
        setFirstName('')
        setMiddleName('')
        setLastName('')
        setGender('')
        setDOB(dayjs('2024-01-01'))
        setAge(0)
    }
    const handleDOB = (newValue: any) => {
        setDOB(newValue)
        setAge(getAge(newValue));
    }
    const getAge = (dateString: number) => {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    const pInfoUpdate: any = useMutation(
        async (data) => {
            return await axios.put(api + "/update/" + isRow.id, data, {
                headers: {
                    Accept: "application/json",
                },
            });
        },
        {
            onSuccess: (res: any) => {
                handleAlert(res.data.message)
                queryClient.invalidateQueries("pInfoData");
                handleClose();
            },
            onError: (error: any) => {
                handleAlert(error.data.message)
                console.log(" error" + error);
            },
        }
    );


    const handleUpdate = (data: any) => {
        // let dt = moment(new Date(dOB)).format('YYYY-MM-DD')
        pInfoUpdate.mutate(data);
    }



    const handleChange = (event: SelectChangeEvent) => {
        setGender(event.target.value as string);
    };

    const handleClose = () => {
        handleIsOpen(false);
        handleIsRow([]);

        // setEditShow(false)
    };


    return (
        <>
            <React.Fragment>
                <Dialog
                    open={isOpen}
                    onClose={handleClose}
                    PaperProps={{
                        component: 'form',
                        onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
                            event.preventDefault();
                            const formData = new FormData(event.currentTarget);
                            const formJson = Object.fromEntries((formData as any).entries());
                            // console.log(formJson)
                            editShow ? handleUpdate(formJson) : handleSave(formJson);
                        },
                    }}
                >
                    <DialogTitle>Register {isRow.Firstname}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To list to this website, please enter your email address here. We
                            will send updates occasionally.
                        </DialogContentText>

                        <Stack spacing={2} sx={{ mt: 3 }} >
                            <TextField
                                autoFocus
                                required
                                fullWidth
                                label="Fist name"
                                id="outlined-size-small1"
                                size="small"
                                name='firstName'
                                value={firstName || ''}
                                onChange={(newValue: any) => setFirstName(newValue.target.value.replace(/[^a-z ]/gi, ''))}
                            />

                            <TextField
                                fullWidth
                                label="Middle name"
                                id="outlined-size-small2"
                                size="small"
                                name='middleName'
                                value={middleName || ''}
                                onChange={(newValue: any) => setMiddleName(newValue.target.value.replace(/[^a-z ]/gi, ''))}
                            />

                            <TextField
                                autoFocus
                                type='text'
                                required
                                fullWidth
                                label="Last name"
                                id="outlined-size-small3"
                                size="small"
                                name='lastName'
                                value={lastName || ''}
                                onChange={(newValue: any) => setLastName(newValue.target.value.replace(/[^a-z ]/gi, ''))}
                            />

                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label2" size='small' required>Gender</InputLabel>
                                <Select
                                    required
                                    labelId="demo-simple-select-label2"
                                    size='small'
                                    id="demo-simple-select4"
                                    value={gender || ''}
                                    label="Gender"
                                    name='gender'
                                    onChange={handleChange}
                                >
                                    <MenuItem value={"Male"}>Male</MenuItem>
                                    <MenuItem value={"Female"}>Female</MenuItem>
                                </Select>
                            </FormControl>

                            <LocalizationProvider dateAdapter={AdapterDayjs} >
                                <DatePicker
                                    slotProps={{ textField: { size: 'small', required: true, helperText: "YYYY/DD/MM" } }}
                                    label={'Date of Birth'}
                                    defaultValue={dOB}
                                    name='dateOfBirth'
                                    value={dOB || '2014-01-01'}
                                    onChange={(newValue) => handleDOB(newValue)}
                                    format="YYYY-MM-DD"
                                />
                            </LocalizationProvider>

                            <TextField
                                required
                                fullWidth
                                label="Age"
                                // type='number'
                                value={age || ''}
                                InputProps={{ inputProps: { min: 0, max: 150 } }}
                                onChange={(val: any) => setAge(val.target.value.replace(/[^0-9]/gi, ''))}
                                id="outlined-size-small5"
                                size="small"
                                name='age'
                            />

                        </Stack>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => handleClose()} disabled={pInfoSave.isLoading ? true : false}>Cancel</Button>

                        <Button type="submit" style={{ display: editShow ? 'none' : 'inline' }} >
                            {pInfoSave.isLoading ? <CircularProgress
                                size={15}
                                sx={{
                                    color: grey[500],
                                }}
                            /> :
                                "Save"}</Button>

                        <Button type='submit'
                            disabled={pInfoSave.isLoading ? true : false}
                            style={{ display: editShow ? 'inline' : 'none' }}>{pInfoSave.isLoading ? <CircularProgress
                                size={15}
                                sx={{
                                    color: grey[500],
                                }}
                            /> :
                                "Update"}</Button>

                    </DialogActions>
                </Dialog>
            </React.Fragment >
        </>
    )
}
