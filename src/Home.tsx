import * as React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import { grey } from '@mui/material/colors';
import Skeleton from '@mui/material/Skeleton';
import Grid from '@mui/material/Grid';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';

import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';

import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import IconButton from '@mui/material/IconButton';

import EditIcon from '@mui/icons-material/Edit';

import TextField from '@mui/material/TextField';
import axios from 'axios';
import { useDebounce } from "use-debounce";
import Tooltip from '@mui/material/Tooltip';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';

import './App.css'
import {
    useMutation,
    useQueryClient,
    useQuery,
} from 'react-query'
import RegisterPage from './Register';

import { useTheme } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import TableHead from '@mui/material/TableHead';
import { styled } from '@mui/material/styles';
import NavBar from './NavBar';
import FallbackPage from './Fallback';


const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));


const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.white,
        color: theme.palette.common.black,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

interface TablePaginationActionsProps {
    count: number;
    page: number;
    rowsPerPage: number;
    onPageChange: (
        event: React.MouseEvent<HTMLButtonElement>,
        newPage: number,
    ) => void;
}

function TablePaginationActions(props: TablePaginationActionsProps) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;

    const handleBackButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onPageChange(event, page + 1);
    };


    return (
        <Box sx={{ flexShrink: 0, ml: 2.5 }}>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>

        </Box>
    );
}

export default function HomePage() {
    const api = "http://127.0.0.1:8000/api";

    const [srch, setSrch] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const [id, setID] = React.useState(0);

    const [debouncedValue] = useDebounce(srch, 500);
    const queryClient = useQueryClient();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const pInfoData = useQuery({
        queryKey: [
            "pInfoData",
            { search: debouncedValue },
        ],
        queryFn: async () => {
            var str =
                api + "/pinfo/" +
                srch;

            const res = await fetch(str)
            return res.json()
        },
    })

    const pInfoDelete: any = useMutation(
        async (id: any) => {
            return await axios.delete(api + "/delete/" + id, {
                headers: {
                    Accept: "application/json",
                },
            });
        },
        {
            onSuccess: (res: any) => {
                handleAlert(res.data.message)
                setToOpen(false);
                queryClient.invalidateQueries({ queryKey: ['pInfoData'] });
            },
            onError: (error: any) => {
                handleAlert(error.data.message)
                console.log("error" + error);
            },
        }
    );

    const [isFrom, setIsFrom] = React.useState<string>('');
    const handleDialog = (row: any, from: string) => {
        setToOpen(true);
        setID(row.id)
        setIsFrom(from)
        setRow(row)
    }

    const handleEdit = (row: any) => {
        setEditShow(true)
        setOpen(true)
        setRow(row)

    }

    const [editShow, setEditShow] = React.useState(false);

    const handleIsOpen = (val: any) => {
        setEditShow(val)
        setOpen(val)
        val !== true ? setIsFrom('') : null
    }
    const handleIsRow = (val: any) => {
        setRow(val)
    }

    const [row, setRow] = React.useState([]);


    const [toOpen, setToOpen] = React.useState(false);


    const handleToClose = () => {
        setRow([])
        setIsFrom('');
        setToOpen(false);
    };

    const handleToConfirm = () => {
        setRow([])
        setToOpen(false);
        isFrom === "delete" ? pInfoDelete.mutate(id) : isFrom === "edit" ? handleEdit(row) : null
    };

    // ------

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - pInfoData.data?.data?.length) : 0;

    const handleChangePage = (
        event: React.MouseEvent<any> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const [isAlert, setIsAlert] = React.useState<any>("");
    const [isAlertHidden, setIsAlertHidden] = React.useState<boolean>(true);

    const handleAlert = (val: any) => {
        setIsAlertHidden(true)
        setTimeout(function() {
            setIsAlertHidden(false)
        }, 5000)
        setIsAlert(val)
    }

    return (
        <>
            <NavBar />
            <Container maxWidth="sm">
                <Box >
                    <Typography variant="h6" component="h5" sx={{ my: 2 }}>
                        Personal Information
                    </Typography>
                </Box>
            </Container >
            <React.Fragment>
                <Dialog
                    open={toOpen}
                    onClose={handleToClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title" sx={{ fontSize: ".77rem", fontWeight: 500, letterSpacing: .5 }}>
                        {`Do you want to ${isFrom === 'delete' ? 'delete' : 'edit'} this user ?`}
                    </DialogTitle>
                    <DialogActions>
                        <Button onClick={handleToClose}
                            startIcon={<CloseIcon />} sx={{ color: "grey" }}>Cancel</Button >
                        <Button onClick={handleToConfirm}
                            startIcon={isFrom === 'delete' ? <DeleteOutlineIcon /> : <EditIcon />} autoFocus>
                            Confirm
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>

            <RegisterPage
                isOpen={open}
                isRow={row}
                handleAlert={handleAlert}
                handleIsOpen={handleIsOpen}
                editShow={editShow}
                handleIsRow={handleIsRow} />

            <Box sx={{ flexGrow: 1 }}>
                <Grid container sx={{ padding: 1, }}>
                    <Grid item xs={7} >
                        {
                            isAlert === "success" ?
                                <Item sx={{
                                    mt: 1, width: "50%",
                                    fontWeight: 500, letterSpacing: 1, display: isAlertHidden ? '' : 'none'
                                }} >
                                    <Stack spacing={2} sx={{ mt: 0, color: "black" }} direction="row">
                                        <CheckCircleOutlineIcon sx={{ color: "teal" }} /> {isAlert}
                                    </Stack>
                                </Item>
                                // <Alert severity="success" sx={{ display: isAlertHidden ? '' : 'none' }}>{isAlert}</Alert>
                                : isAlert === "success" || isAlert === "Not found" ?
                                    <Item sx={{
                                        mt: 1, width: "50%", fontWeight: 400,
                                        backgroundColor: "#fdeded", letterSpacing: 1, display: !isAlertHidden ? '' : 'none'
                                    }} >
                                        <Stack spacing={2} sx={{ mt: 0, color: "black" }} direction="row">
                                            <ErrorOutlineIcon sx={{ color: "black" }} /> {isAlert}
                                        </Stack>
                                    </Item> : ""
                        }
                    </Grid>
                    <Grid item xs={5} sx={{ padding: 1 }}>
                        <Tooltip title="Search">
                            <TextField
                                label="Search"
                                id="outlined-size-small"
                                size="small"
                                value={srch === '' ? '' : srch}
                                disabled={
                                    pInfoData.isError || pInfoData.isLoading ||
                                        pInfoData.data?.data?.length === undefined ?
                                        true : false}
                                onChange={(newValue: any) => setSrch(newValue.target.value)}
                            />
                        </Tooltip>

                        <Tooltip title="Add new">
                            <span>
                                <Button variant="text" disabled={
                                    pInfoData.isError || pInfoData.isLoading ||
                                        pInfoData.data?.data?.length === undefined ?
                                        true : false} startIcon={<AddIcon />} sx={{ color: 'teal' }} onClick={() => handleClickOpen()}>
                                    Add</Button>
                            </span>
                        </Tooltip>
                    </Grid>
                </Grid>
            </Box>
            <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                <TableContainer component={Paper} sx={{ mt: 5 }}>

                    <Table sx={{
                        '& .MuiTableCell-sizeMedium': {
                            padding: '5px 16px',
                        },
                    }} aria-label="custom pagination table">

                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="right">First name</StyledTableCell>
                                <StyledTableCell align="right">Middle name</StyledTableCell>
                                <StyledTableCell align="right">Last name</StyledTableCell>
                                <StyledTableCell align="right">Gender</StyledTableCell>
                                <StyledTableCell align="right">Date Of Birth</StyledTableCell>
                                <StyledTableCell align="right">Age</StyledTableCell>
                                <StyledTableCell align="center">Action</StyledTableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {
                                pInfoData.isError || pInfoData.isLoading || pInfoData.data?.data?.length === 0 ||
                                    pInfoData.data?.data?.length === undefined ?
                                    < TableRow
                                    >
                                        <TableCell component="th" scope="row">
                                            <Skeleton variant="text" sx={{ fontSize: '1rem', width: "100%" }} />
                                        </TableCell>
                                        <TableCell style={{ width: 160 }} align="right">
                                            <Skeleton variant="text" sx={{ fontSize: '1rem', width: "100%" }} />
                                        </TableCell>
                                        <TableCell style={{ width: 160 }} align="right">
                                            <Skeleton variant="text" sx={{ fontSize: '1rem', width: "100%" }} />
                                        </TableCell>
                                        <TableCell style={{ width: 160 }} align="right">
                                            <Skeleton variant="text" sx={{ fontSize: '1rem', width: "100%" }} />
                                        </TableCell>
                                        <TableCell style={{ width: 100 }} align="right">
                                            <Skeleton variant="text" sx={{ fontSize: '1rem', width: "100%" }} />
                                        </TableCell>
                                        <TableCell style={{ width: 100 }} align="right">
                                            <Skeleton variant="text" sx={{ fontSize: '1rem', width: "100%" }} />
                                        </TableCell>
                                        <TableCell align="center">

                                            <Stack spacing={2} sx={{ mt: 1 }} direction="row">
                                                <Button variant="text" sx={{ color: 'text.primary' }} disabled={true}>
                                                    <Skeleton variant="text" sx={{ fontSize: '1rem', width: "100%" }} />
                                                </Button>
                                                <Button variant="text" sx={{ color: 'text.primary' }} disabled={true}>
                                                    <Skeleton variant="text" sx={{ fontSize: '1rem', width: "100%" }} />
                                                </Button>
                                            </Stack>
                                        </TableCell>
                                    </TableRow>
                                    :
                                    (pInfoData.data?.data?.length > 0
                                        ? pInfoData.data?.data?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        : pInfoData.data?.data
                                    ).map((row: any, ndx: any) => (
                                        <TableRow key={ndx} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                            <TableCell component="th" scope="row">
                                                {row.Firstname.charAt(0).toUpperCase() + row.Firstname.slice(1)}
                                            </TableCell>
                                            <TableCell style={{ width: 160 }} align="right">
                                                {row.Middlename !== null ? row.Middlename.charAt(0).toUpperCase()
                                                    + row.Middlename.slice(1) : " "}
                                            </TableCell>
                                            <TableCell style={{ width: 160 }} align="right">
                                                {row.Lastname.charAt(0).toUpperCase() + row.Lastname.slice(1)}
                                            </TableCell>
                                            <TableCell style={{ width: 160 }} align="right">
                                                {row.Gender}
                                            </TableCell>
                                            <TableCell style={{ width: 160 }} align="right">
                                                {row.DateOfBirth}
                                            </TableCell>
                                            <TableCell style={{ width: 100 }} align="right">
                                                {row.Age}
                                            </TableCell>
                                            <TableCell style={{ width: 100 }} align="right">
                                                <Stack spacing={2} sx={{ mt: 1 }} direction="row">
                                                    <Tooltip title="Edit">
                                                        <IconButton aria-label="delete" color="primary" onClick={() => handleDialog(row, "edit")}>
                                                            {isFrom === "edit" && id === row.id ? <CircularProgress
                                                                size={15}
                                                                sx={{
                                                                    color: grey[500],
                                                                }}
                                                            /> :
                                                                <EditIcon />}
                                                        </IconButton>
                                                    </Tooltip>
                                                    <Tooltip title="Delete">
                                                        <IconButton aria-label="delete" color="primary" onClick={() => handleDialog(row, "delete")}>
                                                            {isFrom === "delete" && id === row.id ? <CircularProgress
                                                                size={15}
                                                                sx={{
                                                                    color: grey[500],
                                                                }}
                                                            /> :
                                                                <DeleteOutlineIcon />}
                                                        </IconButton>
                                                    </Tooltip>
                                                </Stack>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 53 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>

                        <TableFooter>
                            <TableRow>
                                {
                                    pInfoData.isError || pInfoData.isLoading ||
                                        pInfoData.data?.data?.length === undefined ?

                                        <TableCell>
                                            <Stack spacing={2} sx={{ mt: 1, ml: 5, width: 100 }} direction="row">
                                                <Skeleton variant="text" sx={{ fontSize: '1rem', width: "150%" }} />
                                            </Stack>
                                        </TableCell>
                                        :
                                        <TablePagination
                                            rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                            colSpan={3}
                                            count={pInfoData.data?.data?.length === undefined ? 0 : pInfoData.data?.data?.length}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            onPageChange={handleChangePage}
                                            onRowsPerPageChange={handleChangeRowsPerPage}
                                            ActionsComponent={TablePaginationActions}
                                        />
                                }
                            </TableRow>
                        </TableFooter>

                    </Table>
                </TableContainer>

            </Paper>

            <Stack spacing={2} sx={{ mt: 0 }} direction="row-reverse">
                <p hidden={pInfoData.data?.data?.length === 0
                    ? false : true}
                    style={{
                        fontSize: 10,
                        marginTop: 10,
                        fontWeight: 600,
                        color: "#d64962",
                        letterSpacing: 1.5
                    }}>
                    Not found.</p>
            </Stack>
        </>
    )
}
