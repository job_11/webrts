
import { Outlet, Route, Routes } from 'react-router-dom'
import { Suspense, lazy } from 'react';
import NoMatch from './404.tsx';
import FallbackPage from './Fallback.tsx';
const HomePage = lazy(() => import('./Home.tsx'))


export default function App() {
    // add indicator for new data entry using delay and icon(green color / teal)
    // -------

    return (
        <>
            <Routes>
                <Route element={<Layout />}>
                    <Route index element={
                        <Suspense fallback={<FallbackPage />}>
                            <HomePage />
                        </Suspense>
                    } />
                </Route>
                <Route path="*" element={<NoMatch />} />
            </Routes >
        </>
    )
}


const Layout = () => {
    return (
        <>
            <Outlet />
        </>
    )
};


