import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';

export default function FallbackPage() {
    // add router 
    // add indicator for new data entry using delay and icon(green color / teal)
    // -------


    return (
        <>
            <Box sx={{
                position: 'absolute',
                left: '50%',
                top: '10%',
                transform: 'translate(-50%, -50%)',
                with: "100",
                display: "flex",

            }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant="body2" sx={{ ml: "33%", pl: 0, display: "fixed" }} gutterBottom>
                            loading
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton variant="text" sx={{ fontSize: '1rem', width: "300px" }} />
                    </Grid>
                </Grid>
            </Box>
        </>
    )
}


